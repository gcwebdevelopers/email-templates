# README #

The master template behind all emails sent with Marketo. Based on the [Salted](https://github.com/rodriguezcommaj/salted) project

## The Basics

**all-sections.html** is a super long template with every possible section.

**mkto-template.html** is a Marketo template file. It contains the header and footer of all-sections.html, with a couple .mktoEditable divs inside to hold snippets.

Individual sections are copied into the **sections/** directory, and can be used for Marketo snippets.

**.min.html** files are simply inlined and compressed versions of **.html** files.

## How to Inline and Compress

[Zurb Inliner](zurb.com/ink/inliner.php)

[Html Compressor](https://htmlcompressor.com/compressor/)

